package com.parkit.parkingsystem.service;

import com.parkit.parkingsystem.constants.Fare;
import com.parkit.parkingsystem.model.Ticket;

public class FareCalculatorService {

    public void calculateFare(Ticket ticket){
        if( (ticket.getOutTime() == null) || (ticket.getOutTime().before(ticket.getInTime())) ){
            throw new IllegalArgumentException("Out time provided is incorrect:"+ticket.getOutTime().toString());
        }

        // int inHour = ticket.getInTime().getHours();
        // int outHour = ticket.getOutTime().getHours();

        //TODO: Some tests are failing here. Need to check if this logic is correct
        // int duration = outHour - inHour;
        // Ici le problème était que l'ont calculé la durée de stationnement en heures seulement en ne prenant pas en compte les minutes
        long durationMillis = ticket.getOutTime().getTime() - ticket.getInTime().getTime();
        double durationHours = (double) durationMillis / (60 * 60 * 1000); // convert milliseconds to hours

        // cap the duration at 24 hours
        durationHours = Math.min(durationHours, 24.0);

        switch (ticket.getParkingSpot().getParkingType()) {
            case CAR: {
                double farePerHour = Math.min(durationHours, 1.0) * Fare.CAR_RATE_PER_HOUR; // cap the fare at hourly rate for the first hour
                double farePerDay = Math.max(0, durationHours - 1) * Fare.CAR_RATE_PER_HOUR; // apply hourly rate for the remaining hours
                ticket.setPrice(farePerHour + farePerDay);
                break;
            }
            case BIKE: {
                double farePerHour = Math.min(durationHours, 1.0) * Fare.BIKE_RATE_PER_HOUR; // cap the fare at hourly rate
                double farePerDay = Math.max(0, durationHours - 1) * Fare.BIKE_RATE_PER_HOUR;
                ticket.setPrice(farePerHour + farePerDay);
                break;
            }
            default: throw new IllegalArgumentException("Unkown Parking Type");
        }
    }
}